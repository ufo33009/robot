##@package      mission
# @details      Mission functions
# @version      1.0
# @date         1/2021
# @copyright    Team UFO (Ultra Fit Ostrich)

from pybricks.ev3devices import Motor, TouchSensor, ColorSensor, GyroSensor
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.media.ev3dev import SoundFile
from robot import *

###########################################################
## Function:     Master loop
def master():

    # Define a robot instance
    ufo= robot()

    # Play a sound.
    brick.sound.beep()

    # Keyboard
    brick.light(Color.YELLOW)
    target_angle= 90
    timer= StopWatch()
    timer.pause()
    engage_state= 0
    while True:

        # Button detection
        if Button.LEFT in brick.buttons():
            ufo.right_actuator.run_angle(500, 45)
        elif Button.RIGHT in brick.buttons():
            ufo.right_actuator.run_angle(-500, 45)

        elif Button.UP in brick.buttons():
            ufo.left_actuator.run_angle(500, 45)
            ufo.left_actuator.reset_angle(0)
        elif Button.DOWN in brick.buttons():
            ufo.left_actuator.run_angle(-500, 45)

        elif Button.CENTER in brick.buttons():
            # Indicate busy sign
            brick.light(Color.RED)
            # Wait for a short time after  button is pressed
            wait(500)

            # Unlock motors
            ufo.left_motor.stop(Stop.COAST)
            ufo.right_motor.stop(Stop.COAST)
            ufo.left_actuator.stop(Stop.COAST)
            ufo.right_actuator.stop(Stop.COAST)            

            # Do missions according to attachment colorm
            timer= StopWatch()
            # Missions: 
            if   ufo.attach.color()== Color.RED:
                data.log('\r\nRed')
                mission_red(ufo)
            # Missions: 
            elif ufo.attach.color()== Color.WHITE:
                data.log('\r\nWhite')
                mission_white(ufo)
            # Missions: 
            elif ufo.attach.color()== Color.BLACK:
                data.log('\r\nBlack')
                mission_black(ufo)
            # Missions: 
            elif ufo.attach.color()== Color.GREEN:
                data.log('\r\nGreen')
                mission_green(ufo)                  

            # Indicate ready sign
            brick.light(Color.YELLOW)
            data.log('\r\nMission time: %3.1fs'%(timer.time()*0.001))            

###########################################################
# Program for mission_white
def mission_white(ufo):  

    ufo.gyro.reset_angle(0)
    ufo.left_actuator.reset_angle(0)

    # 1. basket
    ufo.move(144, 300, 0, Stop.COAST)
    ufo.curve(150, 300, -75, Stop.COAST)
    ufo.move(550, 500, -90, Stop.COAST, reset= False)
    ufo.move(800, 200, -90, Stop.COAST, reset= False)    
    ufo.move(1200, 200, -135, Stop.COAST, reset= False)  
    wait(100)

    # 2. health unit
    ufo.move(10, -100, ufo.gyro.angle())
    ufo.left_actuator.run_target(-1500, -360)

    ufo.move(120, -200, -135)
    ufo.move(190, 250, -45)
    ufo.right_actuator.run_angle(400, 180)
    ufo.move(175,-250,-45)

    # 3. aimer
    ufo.move(565,500,0)
    ufo.left_actuator.run_target(-1500, -360*1.8)
    ufo.move(60, -200, 0)

    # 4. slide   
    ufo.move(275, 200, 150, Stop.COAST)
    ufo.move(500, 300, 110, Stop.COAST, reset= False)
    ufo.move(1000, 400, 170, Stop.COAST, reset= False) 

###########################################################
# Program for mission_red if not flip tire
def mission_red(ufo):

    # Start point: X= 17M, Y= 12M, angle= 0deg CW
    ufo.gyro.reset_angle(0)
    ufo.left_actuator.reset_angle(0)

    ufo.move(700, 500, 0, stop_type= Stop.COAST)
    ufo.move(1175, 500, 0, reset= False, stop_type= Stop.COAST)

    ufo.right_actuator.run_angle(1200, 280, wait= False)     
    ufo.move(1400, 400, 0, reset= False, stop_type= Stop.COAST)  
    ufo.move(1600, 300, 0, reset= False) 

    # 1. health unit catcher    
    ufo.right_actuator.run_angle(-500, 400)   
    wait(100) 
    ufo.left_actuator.run_target(-800, -90)
    wait(100)
    ufo.right_actuator.run_angle(400, 360*2, wait= False)
    ufo.move(1635, 250, 0, reset= False) 

    # 2. treadmill
    ufo.right_actuator.run_angle(2000, 360*10) 

    # 3. big tire
    ufo.move(35, -200, 0, stop_type= Stop.COAST)    
    ufo.curve(-150, -200, -86, stop_type= Stop.COAST)     
    ufo.left_actuator.run_target(-800, -180)
    ufo.left_actuator.run_target(-800, -185) 
    ufo.gyro.reset_angle(-90)

    ufo.move(405, 400, -90)    
    ufo.right_actuator.run_angle(-600, 270)
    wait(100)
    ufo.right_actuator.run_angle(600, 330)

    # 4. row machine
    ufo.turn(19)
    ufo.left_actuator.run_target(-800, -270)
    ufo.left_actuator.run_target(-800, -275)           
    wait(250)

    ufo.right_actuator.run_angle(1500, 270)      
    ufo.move(131, 150, 19)

    ufo.right_actuator.run_angle(-1500, 360) 
    ufo.move(125, -100, 19)
    ufo.move(10, 100, 19)
    ufo.turn(25)    
    ufo.right_actuator.run_angle(1500, 450) 

    # 5. small tire
    ufo.move(60, 150, 25)
    ufo.move(55, 100, 180)
    ufo.right_actuator.run_angle(-1500, 360)

    # 6. home
    ufo.move(110, 500, 135, Stop.COAST)    
    ufo.move(1350, 500, 170, Stop.COAST, reset= False) 
    ufo.left_actuator.run_target(-800, -360, wait= False)  
   
###########################################################
# Program for mission_green
def mission_green(ufo):

    ufo.gyro.reset_angle(-90)
    ufo.move(572, 400, -90)    
    ufo.move(350, -400, ufo.gyro.angle(), Stop.COAST)

###########################################################
# Program for mission_blue
def mission_black(ufo):

    # Start point: X= 8M, Y= 5M, angle= 0deg CW
    ufo.gyro.reset_angle(0)
    ufo.left_actuator.reset_angle(0)

    # 1. step counter
    ufo.move(700, 500, 0, Stop.COAST)
    ufo.move(900, 300, 0, Stop.COAST, reset= False)    
    ufo.move(1165,400, 0, reset= False)    

    # 2. square
    ufo.move(75, -200, ufo.gyro.angle())
    ufo.move(40, -200, -90, stop_type= Stop.COAST)
    ufo.gyro.reset_angle(-90)    

    # 3. health unit
    ufo.left_actuator.run_target(500, 660, wait= False)
    ufo.move(280, 300, -90)  
    ufo.left_actuator.run_target(-1000, 260)
    ufo.move(110, -200, -90)
    ufo.left_actuator.run_target(1500, 1000)

    # 4. boccia
    ufo.move(635, 500, -90)   
    ufo.right_actuator.run_angle(500, 120)
    wait(500) 

    # 5. pull-up bar   
    ufo.move(470, -250, -90)