from pybricks.parameters import Stop
from pybricks.tools import wait, StopWatch, DataLog

###########################################################  
## Data logging class    
class data_log():
    ## Constructor
    # @param self         Pointer to this class
    # @param save_to_file Boolean flag for file saving (True)/screen display (False)       
    def __init__(self, save_to_file= False):
        # @var self.save_to_file    Boolean flag for file saving (True)/screen display (False)      
        self.save_to_file= save_to_file
        if self.save_to_file:
            self.log_file = open('data.log', 'a')

    ## Function:    Log data
    # @param self   Pointer to this class
    # @param info   String information to be logged
    def log(self, info):
        if self.save_to_file:
            self.log_file.write(info + '\r\n')
        else:
            print(info)

data= data_log(False)

###########################################################    
## Function:        Slew rate limit for speed or angle
# @param target     Target value [mm/s or deg]
# @param limit      Previous limited value [mm/s or deg]
# @param rate       Rate limit [mm/s/s or deg/s]
# @return           New limited value [mm/s or deg]
def slew(target, limit, accel, decel, dt):
    # Rate per period
    if target >= 0:
        rate_pos= accel * dt
        rate_neg= decel * dt
    else:
        rate_pos= decel * dt
        rate_neg= accel * dt

    # Set speed slew rate
    diff= target- limit
    if diff > rate_pos:
        limit += rate_pos
    elif diff < -rate_neg:
        limit -= rate_neg
    else:
        limit= target

    return(limit)

###########################################################
## Function:        Range limit    
# @param target     Target value
# @param limit      Limit
# @return           Limited value
def sat(target, limit):
    if target > limit:
        return limit
    elif target < -limit:
        return -limit
    else:
        return target

def sat2(target, lower_limit, upper_limit):
    if target > upper_limit:
        return upper_limit
    elif target < lower_limit:
        return lower_limit
    else:
        return target

###########################################################
## Function:        Extrac sign of the input number    
# @param num        Signed number
# @return           -1, 0, or 1
def sign(sig, num= 1):
    if sig > 0:
        return num
    elif sig < 0:
        return -num
    else:
        return 0

###########################################################    
## Function:        Empty function for default callback
# @param            None
# @return           None
def do_nothing(self):
    pass

###########################################################    
# SpeedLimiter class
class ratelimiter():
    def __init__(self, distance, speed_target, init_speed, stop_type):
        
        self.accel_limit = 500 # [mm/s/s]
        self.decel_limit = 500 # [mm/s/s]

        self.distance    = distance
        self.speed_target= speed_target
        self.speed_ref   = init_speed
        self.stop_type   = stop_type

        ## decelerate distance, plus 20% safty coefficient
        self.decelerate_distance= distance - 0.5* speed_target* speed_target/ self.decel_limit

        ## low speed target
        self.low_speed_target= sign(speed_target, min(70, abs(speed_target)))

    def update(self, position, dt):
        # Ramp up speed command
        if position < self.decelerate_distance or self.stop_type== Stop.COAST:
            # For rough control
            self.speed_ref= slew(self.speed_target, self.speed_ref, self.accel_limit, self.decel_limit, dt * 0.001)
        else:
            # For fine control
            self.speed_ref= slew(self.low_speed_target, self.speed_ref, self.accel_limit, self.decel_limit, dt * 0.001)

        return self.speed_ref

###########################################################    
# PID control class 
class pid():
    # Initialize PID controller   
    def __init__(self, kp= 1, ki= 0, kd= 0, limit= 100):
        self.kp= kp
        self.ki= ki
        self.kd= kd
        self.intg= 0
        self.prev= 0
        self.time= 0
        self.dt= 0
        self.limit= limit
        self.started= False

    # Do this every control cycle
    def update(self, ref, fb, ff= 0, dt= 0):
        # Control error
        err= ref -fb

        # Integrator
        self.intg= self.intg+ err* self.ki* dt
        if self.intg> self.limit:
            self.intg= self.limit
        elif self.intg< -self.limit:
            self.intg= -self.limit
                      
        # Control output       
        output= err* self.kp+ self.intg+ ff
        if self.kd != 0 and dt != 0:
            if self.started:
                output= output+ (err- self.prev)* self.kd / dt
            else:
                self.started= True
                     
            # Save previous error
            self.prev= err

        overshoot= output- self.limit
        if overshoot> 0:
            self.intg= self.intg- overshoot
            output= self.limit
        else:
            overshoot= output+ self.limit
            if overshoot< 0:
                self.intg= self.intg- overshoot
                output= -self.limit

        return output