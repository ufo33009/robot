##@package      robot
# @details      Fundamental robot control functions
# @version      2.0
# @date         08/2020
# @copyright    Team UFO (Ultra Fit Ostrich)

from pybricks import ev3brick as brick
from pybricks.ev3devices import Motor, TouchSensor, ColorSensor, GyroSensor
from pybricks.robotics import DriveBase
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.media.ev3dev import SoundFile
from pybricks.media.ev3dev import Font

from pid import *
from mymath import *

###########################################################    
# Robot class
class robot():
    def __init__(self):
        # Initialize linear state and parameters
        self.position    = 0   # [mm]
        self.speed_limit = 500 # [mm/s]
        self.accel_limit = 500 # [mm/s/s]
        self.decel_limit = 500 # [mm/s/s]

        # Initialize dimensional parameters
        # Wheel diameter in mm
        self.wheel_diameter= 80.8 # 2902 tire 81.1mm
        # Wheel distance from number of Lego Unit (1LU= 8mm)
        self.wheel_distance= 14.0 * 8
        
        self.const_rot_to_linear     = self.wheel_diameter * 3.1415927 / 360
        self.const_rot_to_linear_half= self.const_rot_to_linear * 0.5        
        self.const_yaw_to_linear     = self.wheel_distance * 3.1415927 / 360

        self.const_linear_to_rot    = 360 / self.wheel_diameter / 3.1415927

        # Setup motors and sensors, check wire connection
        self.diagnosis()      

        # Initialize parent class
        self.left_wheel = wheel(self.left_motor)
        self.right_wheel= wheel(self.right_motor)

        self.position_estimator= dead_rechoning()

        # Global timer
        self.timer= StopWatch()
        self.time = 0
        self.dt   = 0

        data.log('\r\n-- Program start --\r\n')

    ###########################################################    
    # Function:     Move distance, closed loop control, using gyro sensor
    # Parameters:   distance_target [mm]
    #               speed_target [mm/s]
    #               heading_target [deg]
    def move(self, distance_target, speed_target, heading_target, stop_type= Stop.HOLD, callback= do_nothing, reset= True):

        # max speed limit
        speed_target= sat(speed_target, self.speed_limit)
        distance_target= abs(distance_target)

        # reset sensors
        if reset:
            self.reset()
            # Fine control angle
            if abs(heading_target- self.gyro.angle()) > 1 and not stop_type== Stop.COAST:
                self.turn(heading_target)

        # sampling
        self.time= 0
        self.timer.reset()
        self.sample()

        # setup controllers   
        steering_controller= pid(7, 0.0015)                
        speed_limiter  = ratelimiter(distance_target, speed_target, self.speed, stop_type)
        heading_ref    = self.direction

        # debug information
        data.log('\nMove(%d, %d, %d)'%(distance_target, speed_target, heading_target))
        data.log(' time    x    y posi spdr  spd angl  dcl  dcr')
        data.log('% 5d %4d %4d %4d %4d %4d %4d'%(self.time, self.x, self.y, self.position, self.speed, self.speed, self.direction))

        # speed and steering control
        while self.sample() < distance_target and steering_controller.timer.time() < 10:           
            # get speed reference
            speed_ref= speed_limiter.update(self.position, self.dt)
            # heading_ref= slew(heading_target, heading_ref, 75, 75, self.dt* 0.001)
            heading_ref= slew(heading_target, heading_ref, 55, 55, self.dt* 0.001)

            # steering control
            steering_ref= steering_controller.update(heading_ref, self.direction, 0, self.dt)

            # drive the robot
            self.drive(speed_ref, steering_ref, self.dt)
            callback(self)
            data.log('% 5d %4d %4d %4d %4d %4d %4d %4d %4d'%(self.time, self.x, self.y, self.position, speed_ref, self.speed, self.direction, self.left_wheel.duty, self.right_wheel.duty))

        # stop
        self.stop(stop_type)

    ###########################################################    
    # Function:     Stop the robot
    # Parameters:   
    def stop(self, stop_type= Stop.HOLD):
        # Stop
        self.right_motor.stop(stop_type)         
        self.left_motor.stop(stop_type)
 
        if not stop_type== Stop.COAST:                               
            while not self.left_motor.speed()== 0 or not self.right_motor.speed()== 0:
                self.sample()
                data.log('% 5d %4d %4d %4d %4d %4d %4d %4d %4d'%( \
                    self.time, self.x, self.y, \
                    self.position, 0, self.speed, self.direction, \
                    self.left_wheel.speed, self.right_wheel.speed))  

        # Stop
        self.right_motor.stop(stop_type)         
        self.left_motor.stop(stop_type)

    ###########################################################    
    # Function:     Reset distance counter
    # Parameters:          
    def reset(self):
        self.left_motor.reset_angle(0)
        self.right_motor.reset_angle(0)
        self.position_estimator.d= 0 

    ###########################################################    
    # Function:     Move distance along a line, closed loop control, using color sensors
    # Parameters:   radius [mm]
    #               speed_target [mm/s]
    #               yaw_angle_target [deg]
    def line_follower(self, distance, speed_target, stop_type= Stop.HOLD, callback= do_nothing, reset= True):

        # max speed limit
        speed_target= sat(speed_target, self.speed_limit)
        distance= abs(distance)

        data.log('\nLine_follower(%d, %d)'%(distance, speed_target))
        data.log(' time posi ster left righ')

        # reset sensors
        if reset:
            self.reset()

        ## sampling
        self.time= 0
        self.timer.reset()
        self.sample()

        # setup controller
        steering_controller= pid(.9, .00, 150, limit= speed_target)
        speed_limiter      = ratelimiter(distance, speed_target, self.speed, stop_type)

        data.log('% 5d %4d %4d %4d %4d'%(self.time, self.position, self.speed, self.speed, self.direction))        

        # control loop
        while self.sample() < distance and self.timer.time() < 10:           
            # get speed reference
            speed_ref= speed_limiter.update(self.position, self.dt)

            # steering control
            steering_ref= steering_controller.update(0, -self.left_color.reflection()+ self.right_color.reflection(), 0, self.dt)

            # drive the robot
            self.drive(speed_ref, steering_ref, self.dt)
            callback(self)
            data.log('% 5d %4d %4d %4d %4d'%(self.time, self.position, steering_ref, self.left_color.reflection(), self.right_color.reflection()))

        # stop
        self.stop(stop_type)

    ###########################################################    
    # Function:     Run a curve to a specific direction
    # Parameters:   radius [mm]
    #               speed_target [mm/s]
    #               yaw_angle_target [deg]
    def curve(self, radius, speed_target, yaw_angle_target, stop_type= Stop.HOLD, callback= do_nothing, reset= True):

        data.log('\nCurve(%d, %d, %d)'%(radius, speed_target, yaw_angle_target))
        data.log(' time posi spdr  spd angl')

        # reset sensors
        if reset:
            self.reset()  

        # sampling
        self.time= 0
        self.timer.reset()
        self.sample()
        
        # get initial values of slew rate limited speed and angle
        speed_ref= self.speed
        const_radius_to_steering= self.wheel_distance/ (2* radius)
        print(const_radius_to_steering)
        # steering_ref= speed_ref * const_radius_to_steering * sign(yaw_angle_target - self.direction)

        ## low speed target
        low_speed_target= sign(speed_target, 100)

        # control loop
        angle= yaw_angle_target - self.direction
        if angle > 0:
            while self.direction < yaw_angle_target - 1:
                if self.direction < yaw_angle_target - 30 or stop_type== Stop.COAST:
                    speed_ref= slew(speed_target, speed_ref, self.accel_limit, self.decel_limit, self.dt * 0.001)
                else:
                    speed_ref= slew(low_speed_target, speed_ref, self.accel_limit, self.decel_limit, self.dt * 0.001)

                steering_ref= -speed_ref * const_radius_to_steering                                
                self.drive(speed_ref, steering_ref, self.dt)
                self.sample()
                data.log('% 5d %4d %4d %4d %4d %4d %4d'%( \
                    self.time, self.position, speed_ref, self.speed, self.direction,
                    self.left_wheel.speed, self.right_wheel.speed))
        else:
            while self.direction > yaw_angle_target + 1:
                if self.direction > yaw_angle_target + 30 or stop_type== Stop.COAST:
                    speed_ref= slew(speed_target, speed_ref, self.accel_limit, self.decel_limit, self.dt * 0.001)
                else:
                    speed_ref= slew(low_speed_target, speed_ref, self.accel_limit, self.decel_limit, self.dt * 0.001)

                steering_ref= speed_ref * const_radius_to_steering * sign(angle)                               
                self.drive(speed_ref, steering_ref, self.dt)
                self.sample()                
                data.log('% 5d %4d %4d %4d %4d %4d %4d'%( \
                    self.time, self.position, speed_ref, self.speed, self.direction,
                    self.left_wheel.speed, self.right_wheel.speed))          

        self.stop(stop_type)

    ###########################################################    
    # Function:     Turn to a direction, closed loop control, use gyro sensor
    # Parameters:   yaw_angle_target [deg]
    def turn(self, yaw_angle_target, stop_type= Stop.HOLD, set_time= 200):
        data.log('\nTurn(%d)'%(yaw_angle_target))
        data.log(' time posi angl')

        # reset sensors
        self.reset()

        ## sampling
        self.time= 0
        self.timer.reset()
        self.sample()

        set_timer= StopWatch()
        data.log('%5d %4d %4d'%(self.time, self.position, self.direction))

        # control loop
        angle= yaw_angle_target - self.direction
        while (abs(angle) > 1 or set_timer.time()< set_time) and self.time< 3000 :
            # rough control
            self.sample()            
            angle= yaw_angle_target - self.direction
            if abs(angle) > 1:
                set_timer.reset()

            if abs(angle) > 10:
                speed= sign(angle, sat(50 + (abs(angle)- 15)* 1.0, 200))               
            elif abs(angle) > 1:
                speed= sign(angle, 20)
            else:
                speed= 0

            self.drive(0, speed, self.dt, method)                    
            data.log('%5d %4d %4d %4d'%(self.time, self.position, self.direction, speed))

        # stop
        self.left_motor.stop(stop_type)
        self.right_motor.stop(stop_type)

        # wait(1000)
        self.sample()
        data.log('%5d %4d %4d'%(self.time, self.position, self.direction))

    ###########################################################
    # Function:     Square to wall
    # Parameters:   None
    # Memo:         X range [-330 to 330], Y range [0 to 640]
    #               360*22/12= 660, 360*22/12= 660
    def wall_square(self, angle, backward= True):
        timer= StopWatch()

        if backward:
            speed= -150
        else:
            speed= 150

        self.right_motor.run_time(speed, 1000, wait= False)
        self.left_motor.run_time(speed, 1000)

        # wait(250)     
        self.right_motor.run_time(speed /2, 500)
        self.left_motor.run_time (speed /2, 500)         
        self.gyro.reset_angle(angle)
        data.log('\r\nSquare(%d to %d)'%(self.gyro.angle(), angle))             

    ###########################################################
    # Function:     Check wire connection
    # Note:         No return until wire connection secured
    def diagnosis(self):

        while True:
            state= True
            # initialize motors
            try:
                self.left_motor= Motor(Port.B, Direction.CLOCKWISE)
            except:
                state= False
                print('Check port B connection')               
                brick.sound.file(SoundFile.SIX)

            try:
                self.right_motor= Motor(Port.C, Direction.CLOCKWISE)
            except:
                state= False
                print('Check port C connection')
                brick.sound.file(SoundFile.SEVEN)

            try:
                self.left_actuator= Motor(Port.A, Direction.CLOCKWISE, [20, 12])
                pass
            except:
                state= False
                print('Check port A connection') 
                brick.sound.file(SoundFile.FIVE)

            try:
                self.right_actuator= Motor(Port.D, Direction.CLOCKWISE, [20, 12])
                pass
            except:
                state= False
                print('Check port D connection')
                brick.sound.file(SoundFile.EIGHT)

            # Initialize sensors
            try:
                self.attach= ColorSensor(Port.S3)
                pass
            except:
                state= False
                print('Check port 3 connection')
                brick.sound.file(SoundFile.THREE)

            try:
                self.left_color= ColorSensor(Port.S2)
                pass
            except:
                state= False
                print('Check port 2 connection') 
                brick.sound.file(SoundFile.TWO)

            try:
                self.right_color= ColorSensor(Port.S4)
                pass
            except:
                state= False
                print('Check port 4 connection')
                brick.sound.file(SoundFile.FOUR)

            try:
                self.gyro= GyroSensor(Port.S1, Direction.CLOCKWISE)
                pass
            except:
                state= False
                print('Check port 1 connection')
                brick.sound.file(SoundFile.ONE)
            
            while not self.drift_detect():
                pass

            # Check again after 0.5s on wire connection error
            if state== True:
                print('Wire connection OK')
                break
            else:
                wait(500)

    ###########################################################
    # Function:     Detect gyro drift
    # Return:       True or False
    def drift_detect(self):
        if self.left_motor.speed()== 0 and self.right_motor.speed()== 0:
            self.gyro.reset_angle(0)
            wait(3000)
            if not self.gyro.angle()== 0 and self.left_motor.speed()== 0 and self.right_motor.speed()== 0:
                print('Drifting', self.gyro.angle(), self.gyro.speed())
                # brick.screen.draw_text(0, 0, 'Drifting')  
                brick.sound.file(SoundFile.TEN)                 
                return False
            else:
                return True

    ###########################################################
    # Function:     Speed control to replace DriveBase.drive
    # Return:       None          
    def drive(self, speed, steering, dt):
        
        # wheel speed limit
        if speed+ steering > self.speed_limit:
            speed= self.speed_limit- steering
        elif speed- steering < -self.speed_limit:
            speed= -self.speed_limit+ steering

        # distribute speed to left and right wheels
        left_speed = speed+ steering
        right_speed= speed- steering            

        # feedforward compensation            
        left_ff = 0.158875* left_speed
        right_ff= 0.158875* right_speed

        # speed PID control            
        self.left_wheel.run( left_speed , left_ff , dt)
        self.right_wheel.run(right_speed, right_ff, dt)

        # duty cycle
        self.left_wheel.motor.dc( self.left_wheel.duty)
        self.right_wheel.motor.dc(self.right_wheel.duty) 

    ###########################################################
    # Function:     Motor speed and angle sampling
    # Return:       None
    def sample(self):
        # speed sampling
        self.left_wheel.speed = self.left_motor.speed() * self.const_rot_to_linear
        self.right_wheel.speed= self.right_motor.speed()* self.const_rot_to_linear

        self.speed = (self.left_wheel.speed + self.right_wheel.speed)* 0.5 

        # angle sampling
        self.direction= self.gyro.angle()

        # time stamp
        time_stamp= self.timer.time()
        self.dt= time_stamp- self.time
        self.time= time_stamp

        # position sampling
        self.position= abs(self.left_motor.angle()+ self.right_motor.angle())* self.const_rot_to_linear_half      

        return self.position

###########################################################    
# Wheel class
class wheel():
    def __init__(self, motor):
        # initialize actuator
        self.motor= motor
        
        # initialize speed controller: input speed command, output duty cicle
        self.speed_controller= pid(0.3, 0.003)        

        # initial values
        self.duty= 0     

    def run(self, speed_ref, ff, dt):
        # PID control
        self.duty= self.speed_controller.update(speed_ref, self.speed, ff, dt)